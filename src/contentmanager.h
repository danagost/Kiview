// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2024 danilo agostini <kyre334@gmail.com>
#ifndef CONTENTMANAGER_H
#define CONTENTMANAGER_H
#include "containerviewer.h"
#include <QDebug>
#include <QFileInfo>
#include <QObject>
#include <QQuickItem>
#include <QTemporaryDir>
#include <documentviewer.h>
#include <filesystem>
#include <imageviewer.h>
#include <textviewer.h>
#include <videoviewer.h>
class ContentManager : public QObject
{
    Q_OBJECT
    QML_SINGLETON
    QML_ELEMENT

    Q_DISABLE_COPY(ContentManager)

    Q_PROPERTY(TextViewer *textViewer READ textViewer CONSTANT FINAL)
    Q_PROPERTY(ImageViewer *imageViewer READ imageViewer CONSTANT FINAL)
    Q_PROPERTY(DocumentViewer *documentViewer READ documentViewer CONSTANT FINAL)
    Q_PROPERTY(VideoViewer *videoViewer READ videoViewer CONSTANT FINAL)
    Q_PROPERTY(ContainerViewer *containerViewer READ containerViewer CONSTANT FINAL)
    Q_PROPERTY(QString currentViewer READ currentViewer WRITE setCurrentViewer NOTIFY currentViewerChanged FINAL)
    Q_PROPERTY(QString fileName READ fileName WRITE setFileName NOTIFY fileNameChanged FINAL)
    Q_PROPERTY(QString windowTitle READ windowTitle WRITE setWindowTitle NOTIFY windowTitleChanged FINAL)

public:
    explicit ContentManager(QObject *parent = nullptr);

    void loadData(const QString &rootDirectory, const QStringList &filesList, const QString &selectedFile);
    Q_INVOKABLE void openApp();
    Q_INVOKABLE void closing();

    TextViewer *textViewer() const;
    ImageViewer *imageViewer() const;
    DocumentViewer *documentViewer() const;
    VideoViewer *videoViewer() const;
    ContainerViewer *containerViewer() const;

    QString currentViewer() const;
    void setCurrentViewer(const QString &newCurrentViewer);

    QString fileName() const;
    void setFileName(const QString &newFileName);

    QString windowTitle() const;
    void setWindowTitle(const QString &newWindowTitle);

Q_SIGNALS:
    void currentViewerChanged();

    void fileNameChanged();

    void windowTitleChanged();
    // Shows the application. In daemon mode the application is hidden instead of closed
    // (hidden, not minimized) and must be unhided when needed.
    void showKiview();

    // Quit application from QML: For some reason "QApplication::quit();"
    // (C++) does not trigger the Close event and so I can't reject it.
    // The same goes for "Qt.quit()" (Qml), that's why root.close() is used instead.
    void closeQml();

private:
    TextViewer *m_textViewer = new TextViewer;
    DocumentViewer *m_documentViewer = new DocumentViewer;
    VideoViewer *m_videoViewer = new VideoViewer;
    ImageViewer *m_imageViewer = new ImageViewer;
    ContainerViewer *m_containerViewer = new ContainerViewer;

    QTemporaryDir tempDir;
    void stopViewers();
    void loadFile(const QFileInfo &fileInfo);
    QString getFileExtension(const QFileInfo &fileInfo);
    QFileInfo currentFileInfo;
    QFileInfo rootDirInfo;

    QStringList m_files;
    QString m_currentViewer;
    QString m_fileName;
    QString m_windowTitle;
};

#endif // CONTENTMANAGER_H
