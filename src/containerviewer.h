// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2024 danilo agostini <kyre334@gmail.com>
#ifndef CONTAINERVIEWER_H
#define CONTAINERVIEWER_H

#include <KArchiveDirectory>
#include <QDir>
#include <QFileInfo>
#include <QObject>

class ElementItem
{
    // Gadget class to pass file information to Qml more easily.
    Q_GADGET
    Q_PROPERTY(QString path READ path WRITE setPath)
    Q_PROPERTY(FileTypes fileType READ fileType WRITE setFileType)
public:
    enum FileTypes { File, Dir, Symlink };
    Q_ENUM(FileTypes)

    bool operator==(const ElementItem &other) const
    {
        return (path() == other.m_path) && (fileType() == other.m_fileType);
    }

    QString path() const
    {
        return m_path;
    }
    void setPath(const QString &path)
    {
        m_path = path;
    }

    FileTypes fileType() const
    {
        return m_fileType;
    }
    void setFileType(FileTypes newFileType)
    {
        m_fileType = newFileType;
    }

private:
    QString m_path;
    FileTypes m_fileType;
};
Q_DECLARE_METATYPE(ElementItem)

class ContainerViewer : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QList<ElementItem> viewerElements READ viewerElements WRITE setViewerElements NOTIFY viewerElementsChanged FINAL)
    Q_PROPERTY(QString fileIcon READ fileIcon WRITE setFileIcon NOTIFY fileIconChanged FINAL)
    Q_PROPERTY(QString fileSize READ fileSize WRITE setFileSize NOTIFY fileSizeChanged FINAL)
    Q_PROPERTY(QString fileSizeUnit READ fileSizeUnit WRITE setFileSizeUnit NOTIFY fileSizeUnitChanged FINAL)
    Q_PROPERTY(QString fileContentCount READ fileContentCount WRITE setFileContentCount NOTIFY fileContentCountChanged FINAL)
    Q_PROPERTY(QString fileLastModified READ fileLastModified WRITE setFileLastModified NOTIFY fileLastModifiedChanged FINAL)
    Q_PROPERTY(QString errorMessageText READ errorMessageText WRITE setErrorMessageText NOTIFY errorMessageTextChanged FINAL)
    Q_PROPERTY(bool loadingBarVisible READ loadingBarVisible WRITE setLoadingBarVisible NOTIFY loadingBarVisibleChanged FINAL)

public:
    explicit ContainerViewer(QObject *parent = nullptr);
    void loadFile(const QFileInfo &fileInfo, bool isArchive);
    void loadDir(const QString &directoryPath);
    void loadArchive(const QFileInfo &fileInfo);
    bool isActiveViewer = false;
    void stopViewer();

    QString currentFilePath;

    std::tuple<float, QString> convertFileSize(const qint64 &size);

    QString fileIcon() const;
    void setFileIcon(const QString &newFileIcon);

    QString fileSize() const;
    void setFileSize(const QString &newFileSize);

    QString fileSizeUnit() const;
    void setFileSizeUnit(const QString &newFileSizeUnit);

    QString fileContentCount() const;
    void setFileContentCount(const QString &newFileContentCount);

    QString fileLastModified() const;
    void setFileLastModified(const QString &newFileLastModified);

    QString errorMessageText() const;
    void setErrorMessageText(const QString &newErrorMessageText);

    QList<ElementItem> viewerElements() const;
    void setViewerElements(const QList<ElementItem> &newViewerElements);

    bool loadingBarVisible() const;
    void setLoadingBarVisible(bool newLoadingBarVisible);

Q_SIGNALS:

    void fileIconChanged();

    void fileSizeChanged();

    void fileSizeUnitChanged();

    void fileContentCountChanged();

    void fileLastModifiedChanged();

    void errorMessageTextChanged();

    void viewerElementsChanged();

    void loadingBarVisibleChanged();

private:
    QStringList recursiveListEntries(const KArchiveDirectory *dir, const QString &path = QString());
    void resetView();
    void loadingFailed(const QString &errorMessage);
    QString m_fileIcon;
    QString m_fileSize;
    QString m_fileSizeUnit;
    QString m_fileContentCount;
    QString m_fileLastModified;
    QString m_errorMessageText;
    QList<ElementItem> m_viewerElements;
    bool m_loadingBarVisible;
};

#endif // CONTAINERVIEWER_H
