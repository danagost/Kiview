// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2024 danilo agostini <kyre334@gmail.com>
#ifndef IMAGEVIEWER_H
#define IMAGEVIEWER_H
#include <QFileInfo>
#include <QObject>
class ImageViewer : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString viewerImage READ viewerImage WRITE setViewerImage NOTIFY viewerImageChanged FINAL)
    Q_PROPERTY(QString errorMessageText READ errorMessageText WRITE setErrorMessageText NOTIFY errorMessageTextChanged FINAL)

public:
    explicit ImageViewer(QObject *parent = nullptr);
    void loadFile(const QFileInfo &fileInfo, const QString &tempDir = QString());
    QString currentFilePath;
    bool isActiveViewer = false;
    void stopViewer();

    QString viewerImage() const;
    void setViewerImage(const QString &newViewerImage);
    void extractPreview(QString path, QString outputDir);

    QString errorMessageText() const;
    void setErrorMessageText(const QString &newErrorMessageText);

Q_SIGNALS:

    void viewerImageChanged();

    void errorMessageTextChanged();

private:
    QString m_viewerImage;
    QString m_errorMessageText;
};

#endif // IMAGEVIEWER_H
