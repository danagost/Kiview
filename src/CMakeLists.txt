#SPDX-License-Identifier: BSD-2-Clause
#SPDX-FileCopyrightText: 2024 danilo agostini <kyre334@gmail.com>
add_executable(kiview
)



target_sources(kiview PRIVATE main.cpp resources.qrc
    contentmanager.h contentmanager.cpp
    textviewer.h textviewer.cpp
    imageviewer.h imageviewer.cpp
    documentviewer.h documentviewer.cpp
    videoviewer.h videoviewer.cpp
    containerviewer.h containerviewer.cpp
    dbusinterface.h dbusinterface.cpp
)


target_link_libraries(kiview
    Qt6::Quick
    Qt6::Qml
    Qt6::Gui
    Qt6::QuickControls2
    Qt6::Widgets
    KF6::I18n
    KF6::CoreAddons
    KF6::Archive
    KF6::DBusAddons
    Qt6::DBus
    Qt6::Concurrent
    Qt6::Pdf
    Qt6::Multimedia

)

install(PROGRAMS io.github.nyre221.kiview.desktop DESTINATION ${KDE_INSTALL_APPDIR})
install(FILES io.github.nyre221.kiview.appdata.xml DESTINATION ${KDE_INSTALL_METAINFODIR})
install(TARGETS kiview ${KDE_INSTALL_TARGETS_DEFAULT_ARGS})
