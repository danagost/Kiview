// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2024 danilo agostini <kyre334@gmail.com>
#include "documentviewer.h"
#include <filesystem>
#include <iostream>
#include <klocalizedstring.h>
#include <signal.h>

// Qt
#include <QDebug>
#include <QProcess>
#include <QRegularExpression>
#include <QStandardPaths>
#include <QtConcurrent>

using namespace Qt::Literals::StringLiterals;

DocumentViewer::DocumentViewer(QObject *parent)
    : QObject{parent}
{
    // gets the libreoffice command to use with QProcess.
    libreofficeCommand = getLibreofficeCommand();
}

void DocumentViewer::loadFile(const QFileInfo &fileInfo, const QString &tempDir)
{
    closeActiveConversionProcess();
    showLoadingScreen();
    currentFilePath = fileInfo.absoluteFilePath();
    // it is used to discard the thread if not longer needed
    isActiveViewer = true;

    // Checks whether the file type requires conversion.
    QString extension = fileInfo.suffix().toLower();
    if (extension.contains(QStringLiteral("pdf"))) {
        setViewerDocument(QStringLiteral("file://") + currentFilePath);
        setMessagePageVisible(false);
        setLoadingBarVisible(false);
    } else {
        // uses QtConcurrent to start the function in another thread.
        QtConcurrent::run(&DocumentViewer::convertDocument, this, currentFilePath, tempDir);
    }
}

void DocumentViewer::convertDocument(QString path, QString outputDir)
{
    // # shows a message and stops the function if libreoffice was not found.
    if (libreofficeCommand.isEmpty()) {
        loadingFailed(DocumentViewer::LoadingError::MissingLibreoffice);
        qWarning() << "libreoffice not found";
        return;
    }

    closeActiveConversionProcess();

    // uses QProcess to convert the file to Pdf via Libreoffice.
    QStringList libreofficeArgs = QStringList() << QStringLiteral("run") << QStringLiteral("run org.libreoffice.LibreOffice") << QStringLiteral("--headless")
                                                << QStringLiteral("--nolockcheck") << QStringLiteral("--norestore") << QStringLiteral("--convert-to")
                                                << QStringLiteral("pdf") << path << QStringLiteral("--outdir") << outputDir;
    QProcess process;

    process.setProgram(libreofficeCommand);

    if (isLibreofficeFlatpak) {
        process.setArguments(QStringList() << QStringLiteral("run") << QStringLiteral("org.libreoffice.LibreOffice") << libreofficeArgs);
    } else {
        process.setArguments(libreofficeArgs);
    }
    process.start();
    process.waitForStarted();
    currentProcessId = process.processId();
    processIsRunning = true;
    bool exitStatus = process.waitForFinished(10000);
    processIsRunning = false;

    // discard the thread if it is no longer needed.
    if (path != currentFilePath || isActiveViewer == false) {
        return;
    }
    if (exitStatus) {
        // all Ok
    } else {
        if (FirstLoading) {
            // It is used to mitigate a problem/bug that occurs when the PC has just
            // been started and the waiting time (waitForFinished) of QProcess is
            // not respected.
            // To try converting the document one more time.
            FirstLoading = false;
            qDebug() << QStringLiteral("retry FirstLoading");
            convertDocument(path, outputDir);
            return;
        }
        // timeout
        loadingFailed(DocumentViewer::LoadingError::Timeout);
        qDebug() << "error:file took to long to load";
        return;
    }
    QString result = QString::fromUtf8(process.readAllStandardOutput());
    QString standardError = QString::fromUtf8(process.readAllStandardError());
    // removes the warning part of the text (always present with libreoffice)
    // and the newline character because otherwise the html link that is
    // inserted after it doesn't work.
    int pos = standardError.indexOf(QStringLiteral("Error"));
    standardError = standardError.mid(pos).trimmed();

    if (result.isEmpty()) {
        if (standardError.isEmpty()) {
            // sometimes the standardOutput and error returns null if you quickly
            // close and reopen Kview. I think it's caused by flatpak.
            qDebug() << "Unknown error";
            // Reload the file.
            reloadDocument();

        } else {
            // send libreoffice error
            // 999: customError
            loadingFailed(DocumentViewer::LoadingError::ErrorConverting, standardError);
            qDebug() << "no results error:" << standardError;
        }
        return;
    }

    // Regular expression to find path between ">" and " using filter"
    static QRegularExpression pathRegex(QStringLiteral(">(.*?) using filter"));
    QRegularExpressionMatch match = pathRegex.match(result);
    QString pdfPath;
    if (match.hasMatch()) {
        // Extract captured group (path) and remove potential leading/trailing spaces
        pdfPath = match.captured(1).trimmed();
        qDebug() << "pdfPath:" << pdfPath;
    }

    // check if the path exists
    QFileInfo pdfFileInfo(pdfPath);
    if (!pdfFileInfo.exists()) {
        qDebug() << "new PDF file does not exist";
        loadingFailed(DocumentViewer::LoadingError::FileNotFound);
        return;
    }

    conversionFinished(pdfFileInfo);
}

void DocumentViewer::conversionFinished(QFileInfo pdfFileInfo)
{
    // calls the function with the new PDF file
    loadFile(pdfFileInfo);
}

void DocumentViewer::stopViewer()
{
    isActiveViewer = false;
    closeActiveConversionProcess();
}

void DocumentViewer::showLoadingScreen()
{
    setMessagePageVisible(true);
    setMessageHeaderText(QStringLiteral(""));
    setMessageDescriptionText(QStringLiteral(""));
    setLoadingBarVisible(true);
    setReloadButtonVisible(false);
    setHelpPageLinkVisible(false);
}

void DocumentViewer::reloadDocument()
{
    loadFile(QFileInfo(currentFilePath));
}

void DocumentViewer::closeActiveConversionProcess()
{
    if (processIsRunning == true) {
        if (isLibreofficeFlatpak) {
            // When using flatpak, The libreoffice process is a grandchild of the PID provided by QProcess.processId()
            // and it is therefore necessary to obtain its PID in order to terminate it.

            // Creates a new QProcess to execute "ps" and get the PID of the first child.
            QProcess processGetChildPid;
            QStringList getFirstChildArgs;
            getFirstChildArgs << u"--ppid"_s << QString::number(currentProcessId) << u"-o"_s << u"pid"_s << u"--no-heading"_s;
            processGetChildPid.start(u"ps"_s, getFirstChildArgs);
            processGetChildPid.waitForFinished();
            // Gets the pid from the process output
            QString firstChildPid = QString::fromUtf8(processGetChildPid.readAllStandardOutput()).trimmed();

            // Gets the child of the first child process (Libreoffice)
            QStringList getSecondChildArgs;
            getSecondChildArgs << u"--ppid"_s << firstChildPid << u"-o"_s << u"pid"_s << u"--no-heading"_s;
            processGetChildPid.start(u"ps"_s, getSecondChildArgs);
            processGetChildPid.waitForFinished();
            // Gets the pid from the process output
            QString secondChildPid = QString::fromUtf8(processGetChildPid.readAllStandardOutput()).trimmed();

            // Closes the libreoffice process.
            kill(secondChildPid.toInt(), SIGTERM);

        } else {
            kill(currentProcessId, SIGTERM);
        }
        processIsRunning = false;
    }
}

QString DocumentViewer::getLibreofficeCommand()
{
    // checks which version of libreoffice is installed.
    QString command = QStringLiteral("");

    if (!QStandardPaths::findExecutable(QStringLiteral("libreoffice")).isEmpty()) {
        // check if libreoffice is installed
        command = QStringLiteral("libreoffice");
        isLibreofficeFlatpak = false;
    } else if (!QStandardPaths::findExecutable(QStringLiteral("flatpak")).isEmpty()) {
        // check if libreoffice is installed with flatpak
        QProcess process;
        process.start(QStringLiteral("flatpak"), {QStringLiteral("info"), QStringLiteral("org.libreoffice.LibreOffice")});
        process.waitForFinished();
        if (process.exitCode() == EXIT_SUCCESS) {
            isLibreofficeFlatpak = true;
            command = QStringLiteral("flatpak");
        }
    }
    return command;
}

void DocumentViewer::loadingFailed(LoadingError error, QString customError)
{
    // code 1: file took too long to load
    // code 2: general error
    // code 3: flatpak error
    // code 4: libreoffice missing
    // code 999: libreoffice error message
    setLoadingBarVisible(false);
    setMessageHeaderText(i18n("Preview not available."));

    if (error == Timeout) {
        setMessageDescriptionText(i18n("The document took too long to load."));
        setReloadButtonVisible(true);
    } else if (error == UnknownError) {
        // general message and reload option
        setMessageDescriptionText(i18n("Something went wrong."));
        setReloadButtonVisible(true);
    } else if (error == FileNotFound) {
        // new pdf not found/flatpak error
        setHelpPageLinkVisible(true);
        setMessageDescriptionText(i18n("File not found: Flatpak may not be configured correctly:"));
    } else if (error == MissingLibreoffice) {
        // libreoffice not found
        setHelpPageLinkVisible(true);
        setMessageDescriptionText(i18n("Libreoffice is not installed:"));
    } else if (error == ErrorConverting) {
        // libre error/general error: source could not be loaded
        setHelpPageLinkVisible(true);
        setMessageDescriptionText(customError + QStringLiteral(":"));
    }
}

QString DocumentViewer::viewerDocument() const
{
    return m_viewerDocument;
}

void DocumentViewer::setViewerDocument(const QString &newViewerDocument)
{
    if (m_viewerDocument == newViewerDocument)
        return;
    m_viewerDocument = newViewerDocument;
    Q_EMIT viewerDocumentChanged();
}

QString DocumentViewer::messageHeaderText() const
{
    return m_messageHeaderText;
}

void DocumentViewer::setMessageHeaderText(const QString &newMessageHeaderText)
{
    if (m_messageHeaderText == newMessageHeaderText)
        return;
    m_messageHeaderText = newMessageHeaderText;
    Q_EMIT messageHeaderTextChanged();
}

bool DocumentViewer::messagePageVisible() const
{
    return m_messagePageVisible;
}

void DocumentViewer::setMessagePageVisible(bool newMessagePageVisible)
{
    if (m_messagePageVisible == newMessagePageVisible)
        return;
    m_messagePageVisible = newMessagePageVisible;
    Q_EMIT messagePageVisibleChanged();
}

QString DocumentViewer::messageDescriptionText() const
{
    return m_messageDescriptionText;
}

void DocumentViewer::setMessageDescriptionText(const QString &newMessageDescriptionText)
{
    if (m_messageDescriptionText == newMessageDescriptionText)
        return;
    m_messageDescriptionText = newMessageDescriptionText;
    Q_EMIT messageDescriptionTextChanged();
}

bool DocumentViewer::reloadButtonVisible() const
{
    return m_reloadButtonVisible;
}

void DocumentViewer::setReloadButtonVisible(bool newReloadButtonVisible)
{
    if (m_reloadButtonVisible == newReloadButtonVisible)
        return;
    m_reloadButtonVisible = newReloadButtonVisible;
    Q_EMIT reloadButtonVisibleChanged();
}

bool DocumentViewer::loadingBarVisible() const
{
    return m_loadingBarVisible;
}

void DocumentViewer::setLoadingBarVisible(bool newLoadingBarVisible)
{
    if (m_loadingBarVisible == newLoadingBarVisible)
        return;
    m_loadingBarVisible = newLoadingBarVisible;
    Q_EMIT loadingBarVisibleChanged();
}

bool DocumentViewer::helpPageLinkVisible() const
{
    return m_helpPageLinkVisible;
}

void DocumentViewer::setHelpPageLinkVisible(bool newHelpPageLinkVisible)
{
    if (m_helpPageLinkVisible == newHelpPageLinkVisible)
        return;
    m_helpPageLinkVisible = newHelpPageLinkVisible;
    Q_EMIT helpPageLinkVisibleChanged();
}
