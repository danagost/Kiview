// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2024 danilo agostini <kyre334@gmail.com>
import QtQuick
import QtQuick.Controls
import CManager 1.0
import org.kde.kirigami as Kirigami

ScrollView {
    anchors.fill: parent
    // Property to enable/disable unloading of the viewer when not in use.
    property bool isPersistent: false
    property list<Kirigami.Action> actions
    //these actions are read by viewersLoader and are shown in the "header bar" of the application.
    actions: [
        OpenAppButton {}
    ]

    // When a key is pressed, return focus to the parent: This is done because focusPolicy,etc,
    // are not respected when opening the context menu (right-click menu).
    Keys.onPressed: event => {
        parent.focus = true;
    }
    TextArea {
        id: textArea
        // Avoids taking focus: The arrow keys are used to scroll through files
        // and TextArea uses the same keys to navigate through the text.
        // This causes conflicts and makes scrolling through files not work.
        activeFocusOnPress: false
        activeFocusOnTab: false
        focusPolicy: Qt.NoFocus
        text: Manager.textViewer.viewerText
        placeholderText: qsTr("placeholder")
        readOnly: true
    }
}
