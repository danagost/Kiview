// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2024 danilo agostini <kyre334@gmail.com>
import QtQuick
import QtCore
import QtQuick.Window
import QtQuick.Controls
import QtMultimedia
import CManager 1.0
import org.kde.kirigami as Kirigami

// Videos currently fail to play in Qt6/Qml via "QtMultimedia" on some Linux distributions.
// The reason seems to be that you need gstreamer 1.22 with qml6glsink and they are currently not available on kde neon (ubuntu 22.04).
Item {
    anchors.fill: parent
    // Property to enable/disable unloading of the viewer when not in use.
    property bool isPersistent: false
    Settings {
        id: settings
        property alias volume: videoFrame.volume
    }
    // Hides the volume slider when the visibility changes.
    // This is to prevent the slider from remaining open when a user changes files.
    onVisibleChanged: volumeSlider.visible = false
    property list<Kirigami.Action> actions
    //these actions are read by viewersLoader and are shown in the "header bar" of the application.
    actions: [
        OpenAppButton {}
    ]

    Text {
        id: errorMessage
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        visible: videoFrame.errorString !== ""
        text: videoFrame.errorString
        color: Kirigami.Theme.textColor
        font.pointSize: 20
    }

    Video {
        id: videoFrame
        focus: true
        volume: settings.volume

        // Background for audio files.
        Rectangle {
            id: videoFrameBackground
            anchors.fill: parent
            color: Kirigami.Theme.backgroundColor
            visible: videoFrame.errorString === "" && videoFrame.seekable && !videoFrame.hasVideo
            Kirigami.Icon {
                id: placeholderIcon
                anchors {
                    top: parent.top
                    horizontalCenter: parent.horizontalCenter
                    verticalCenter: parent.verticalCenter
                }
                height: parent.height / 3
                width: parent.width / 3
                source: "media-default-album"
            }
        }

        // Pauses the media if the viewer is hidden and resumes playback if the source has not changed.
        onVisibleChanged: {
            if (!visible && seekable) {
                videoFrame.pause();
            } else if (visible && videoFrame.playbackState === MediaPlayer.PausedState) {
                videoFrame.play();
            }
        }

        source: Manager.videoViewer.viewerMedia
        onSourceChanged: play()

        anchors {
            right: parent.right
            left: parent.left
            top: parent.top
            bottom: lineSeparator.top
        }

        onPositionChanged: {
            if (!videoSlider.pressed)
                videoSlider.value = videoFrame.position;
        }

        onPlaybackStateChanged: {
            if (videoFrame.playbackState === MediaPlayer.StoppedState && videoFrame.position === videoFrame.duration)
                videoFrame.play();
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                togglePlay();
                volumeSlider.visible = false;
            }
        }
    }

    Kirigami.Separator {
        id: lineSeparator
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: controlBar.top
    }

    Item {
        id: controlBar
        visible: (videoFrame.errorString === "" ? true : false)
        anchors {
            right: parent.right
            left: parent.left
            bottom: parent.bottom
        }

        MouseArea {
            // This area is used to hide the volume slider (if visible/open)
            // if the user clicks on an empty spot in the control bar.
            anchors.fill: parent
            onClicked: volumeSlider.visible = false
        }

        height: 30
        Rectangle {
            id: controlBarBackground
            anchors.fill: parent
            color: Kirigami.Theme.alternateBackgroundColor
        }
        Button {
            id: togglePlayButton
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            flat: true
            onPressed: {
                togglePlay();
                volumeSlider.visible = false;
            }
            icon.name: (videoFrame.playbackState === MediaPlayer.PlayingState ? "media-playback-pause" : "media-playback-start")
        }

        Slider {
            id: videoSlider
            // Avoids taking focus: The arrow keys are used to scroll through files and Slider{}
            // uses the same keys to go forward/backward in the track.
            // This causes conflicts and makes scrolling through files not work.
            focusPolicy: Qt.NoFocus
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: togglePlayButton.right
            anchors.right: volumeButton.left
            from: 0
            to: videoFrame.duration

            onPressedChanged: {
                volumeSlider.visible = false;
                if (!videoSlider.pressed) {
                    videoFrame.seek(videoSlider.value);
                }
            }
        }

        Button {
            id: volumeButton
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: infoSection.left
            flat: true
            onPressed: volumeSlider.visible = !volumeSlider.visible
            icon.name: "player-volume"
            Slider {
                id: volumeSlider
                anchors.horizontalCenter: parent.horizontalCenter
                visible: false
                // The focus is rejected to prevent the internal shortcuts of Slider{}
                // from causing conflicts with those of kiview.
                //(in the future all the arrow keys will be used to change the selected file)
                focusPolicy: Qt.NoFocus
                orientation: Qt.Vertical
                y: parent.y - height
                from: 0
                to: 1
                value: videoFrame.volume
                onValueChanged: videoFrame.volume = value
            }
        }

        Item {
            id: infoSection
            anchors.verticalCenter: parent.verticalCenter
            anchors.rightMargin: 15
            anchors.right: parent.right
            width: (totalTime.width * 2) + separator.width
            Text {
                id: currentTime
                anchors.right: separator.left
                anchors.verticalCenter: parent.verticalCenter
                color: Kirigami.Theme.textColor
                text: millisToMinutesAndSeconds(videoFrame.position)
            }
            Text {
                id: separator
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                color: Kirigami.Theme.textColor
                text: "/"
            }
            Text {
                id: totalTime
                anchors.left: separator.right
                anchors.verticalCenter: parent.verticalCenter
                color: Kirigami.Theme.textColor
                text: millisToMinutesAndSeconds(videoFrame.duration)
            }
        }
    }

    // To avoid conflicts, shortcuts must be disabled when the viewer is not visible
    Shortcut {
        enabled: visible
        sequences: ["w"]
        onActivated: (videoFrame.position + 10000 <= videoFrame.duration ? videoFrame.seek(videoFrame.position + 10000) : videoFrame.seek(videoFrame.duration))
    }
    Shortcut {
        enabled: visible
        sequences: ["s"]
        onActivated: (videoFrame.position - 10000 >= 0 ? videoFrame.seek(videoFrame.position - 10000) : videoFrame.seek(0))
    }
    Shortcut {
        enabled: visible
        sequences: ["e"]
        onActivated: togglePlay()
    }

    function togglePlay() {
        if (videoFrame.playbackState === MediaPlayer.PlayingState) {
            videoFrame.pause();
        } else {
            videoFrame.play();
        }
    }

    function millisToMinutesAndSeconds(millis) {
        var minutes = Math.floor(millis / 60000);
        var seconds = ((millis % 60000) / 1000).toFixed(0);
        return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
    }
}
