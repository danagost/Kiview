// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2024 danilo agostini <kyre334@gmail.com>
import QtQuick
import QtQuick.Controls
import CManager 1.0
import org.kde.kirigami as Kirigami

Image {
    id: imageArea
    anchors.fill: parent
    // Property to enable/disable unloading of the viewer when not in use.
    property bool isPersistent: false
    property list<Kirigami.Action> actions
    //these actions are read by viewersLoader and are shown in the "header bar" of the application.
    actions: [
        OpenAppButton {}
    ]
    fillMode: Image.PreserveAspectFit
    source: Manager.imageViewer.viewerImage
    smooth: (imageArea.sourceSize.height > 256 && imageArea.sourceSize.width > 256)

    Text {
        id: errorMessage
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        visible: (Manager.imageViewer.errorMessageText === "" ? false : true)
        text: Manager.imageViewer.errorMessageText
        color: Kirigami.Theme.textColor
        font.pointSize: 15
    }
}
