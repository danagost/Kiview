// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2024 danilo agostini <kyre334@gmail.com>
import QtQuick
import QtQuick.Controls
import CManager 1.0
import DBusInterface
import org.kde.kirigami as Kirigami

// When these buttons are activated,
// a D-Bus signal is sent to notify applications that have invoked Kiview to change the selected file.
Item {
    anchors.fill: parent
    anchors.leftMargin: 5
    anchors.rightMargin: 5

    Button {
        id: buttonBack
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        flat: true
        action: Kirigami.Action {
            icon.name: "arrow-left"
            onTriggered: BusInterface.SendKeyPresses(Qt.Key_Left, 0)
        }
    }

    Button {
        id: buttonForward
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        flat: true
        action: Kirigami.Action {
            icon.name: "arrow-right"
            onTriggered: BusInterface.SendKeyPresses(Qt.Key_Right, 0)
        }
    }
}
