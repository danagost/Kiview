// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2024 danilo agostini <kyre334@gmail.com>
// Own header
#include "dbusinterface.h"
// Qt
#include <QDBusConnection>
#include <QDBusConnectionInterface>

DBusInterface::DBusInterface(ContentManager *contentManager, QObject *parent)
    : QObject{parent}
    , m_contentManager(contentManager)
{
    QDBusConnection::sessionBus().registerObject(QStringLiteral("/Interface"),
                                                 this,
                                                 QDBusConnection::ExportScriptableContents | QDBusConnection::ExportAdaptors
                                                     | QDBusConnection::ExportAllSignal);
    QDBusConnectionInterface *sessionInterface = QDBusConnection::sessionBus().interface();
    if (sessionInterface) {
        sessionInterface->registerService(QStringLiteral("org.kde.kiview"), QDBusConnectionInterface::QueueService);
    }

    // This is used to implement a minimum delay between file changes.
    // This prevents the QML interface from crashing when the user rapidly presses the navigation keys.
    enableScrollingTimer->setSingleShot(true);
    enableScrollingTimer->setInterval(100);
    connect(enableScrollingTimer, &QTimer::timeout, this, &DBusInterface::enableScrolling);
}

QString DBusInterface::setFileContext(const QString appKey, const QString rootDirectory, const QStringList filesList, const QString selectedFile)
{
    QString error;

    // Checks if all the necessary data is present.
    if (appKey.isEmpty()) {
        error = QStringLiteral("Error: The Application Key is missing.");
    } else if (rootDirectory.isEmpty()) {
        error = QStringLiteral(" Error: Root directory not set.");
    } else if (!QFileInfo::exists(rootDirectory)) {
        error = QStringLiteral("Error: The root directory is invalid.");
    } else if (filesList.isEmpty()) {
        error = QStringLiteral("Error: The file list is empty.");
    } else if (!selectedFile.isEmpty() && !QFileInfo::exists(selectedFile)) {
        // A file is selected but does not exist.
        error = QStringLiteral("Error: The selected file is invalid.");
    }

    if (error.isEmpty()) {
        setLinkedAppKey(appKey);
        m_contentManager->loadData(rootDirectory, filesList, selectedFile);
    } else {
        sendErrorReply(QDBusError::InvalidArgs, error);
        qWarning() << "Kiview::setFileContext" << error;
    }

    return error;
}

void DBusInterface::SendKeyPresses(const int &key, const int &modifiers)
{
    if (!scrollingEnabled) {
        return;
    }
    scrollingEnabled = false;
    enableScrollingTimer->start();
    Q_EMIT keyPressesForwarded(key, modifiers);
}

bool DBusInterface::isDaemon() const
{
    return m_isDaemon;
}

void DBusInterface::setIsDaemon(bool newIsDaemon)
{
    if (m_isDaemon == newIsDaemon)
        return;
    m_isDaemon = newIsDaemon;
    Q_EMIT isDaemonChanged();
}

void DBusInterface::enableScrolling()
{
    scrollingEnabled = true;
}

QString DBusInterface::linkedAppKey() const
{
    return m_linkedAppKey;
}

void DBusInterface::setLinkedAppKey(const QString &newLinkedAppKey)
{
    if (m_linkedAppKey == newLinkedAppKey)
        return;
    m_linkedAppKey = newLinkedAppKey;
    Q_EMIT linkedAppKeyChanged();
}
