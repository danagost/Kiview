// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2024 danilo agostini <kyre334@gmail.com>
#include "videoviewer.h"

VideoViewer::VideoViewer(QObject *parent)
    : QObject{parent}
{
}

void VideoViewer::loadFile(const QFileInfo &fileInfo)
{
    setViewerMedia(QStringLiteral("file://") + fileInfo.absoluteFilePath());
}

QString VideoViewer::viewerMedia() const
{
    return m_viewerMedia;
}

void VideoViewer::setViewerMedia(const QString &newViewerMedia)
{
    if (m_viewerMedia == newViewerMedia)
        return;
    m_viewerMedia = newViewerMedia;
    Q_EMIT viewerMediaChanged();
}
