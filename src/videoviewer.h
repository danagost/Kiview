// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2024 danilo agostini <kyre334@gmail.com>
#ifndef VIDEOVIEWER_H
#define VIDEOVIEWER_H

#include <QFileInfo>
#include <QObject>

class VideoViewer : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString viewerMedia READ viewerMedia WRITE setViewerMedia NOTIFY viewerMediaChanged FINAL)

public:
    explicit VideoViewer(QObject *parent = nullptr);
    void loadFile(const QFileInfo &fileInfo);

    QString viewerMedia() const;
    void setViewerMedia(const QString &newViewerMedia);

Q_SIGNALS:

    void viewerMediaChanged();

private:
    QString m_viewerMedia;
};

#endif // VIDEOVIEWER_H
