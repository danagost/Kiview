// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2024 danilo agostini <kyre334@gmail.com>
#include "contentmanager.h"
#include <filesystem>
#include <unistd.h>

// Qt
#include "QApplication"
#include <QDebug>
#include <QDir>
#include <QFileInfo>
#include <QProcess>
QStringList textExtensions = QStringList{QStringLiteral("txt"), QStringLiteral("sh"), QStringLiteral("json"), QStringLiteral("md")};
QStringList docExtensions = QStringList{QStringLiteral("pdf"),
                                        QStringLiteral("doc"),
                                        QStringLiteral("docx"),
                                        QStringLiteral("odt"),
                                        QStringLiteral("ods"),
                                        QStringLiteral("xlsx"),
                                        QStringLiteral("xls"),
                                        QStringLiteral("csv"),
                                        QStringLiteral("odp"),
                                        QStringLiteral("ppt"),
                                        QStringLiteral("pptx")};
QStringList imgExtensions = QStringList{QStringLiteral("png"),
                                        QStringLiteral("jpg"),
                                        QStringLiteral("jpeg"),
                                        QStringLiteral("kra"),
                                        QStringLiteral("svgz"),
                                        QStringLiteral("svg"),
                                        QStringLiteral("webp"),
                                        QStringLiteral("jxl")};
QStringList mediaExtensions = QStringList{QStringLiteral("mp4"),
                                          QStringLiteral("mp3"),
                                          QStringLiteral("webm"),
                                          QStringLiteral("mkv"),
                                          QStringLiteral("m4v"),
                                          QStringLiteral("m4a"),
                                          QStringLiteral("wav"),
                                          QStringLiteral("aif"),
                                          QStringLiteral("aiff"),
                                          QStringLiteral("flac"),
                                          QStringLiteral("ogg")};
QStringList containerExtensions = QStringList{QStringLiteral("zip"), QStringLiteral("gz"), QStringLiteral("xz"), QStringLiteral("7z")};

ContentManager::ContentManager(QObject *parent)
    : QObject{parent}
{
}

void ContentManager::loadData(const QString &rootDirectory, const QStringList &filesList, const QString &selectedFile)
{
    // Shows the kiview window.
    Q_EMIT showKiview();
    // Loads the data received via Dbus.
    // If the selected file matches the root folder, the current index number is set to 0 in the GUI.
    rootDirInfo = QFileInfo(rootDirectory);
    // The list of files to be displayed is the one received via DBus
    m_files = filesList;
    QFileInfo fileInfo;
    // If no file has been selected (no selection) shows a preview of the root folder.
    if (selectedFile.isEmpty()) {
        // The file to preview is the "root" folder.
        fileInfo = rootDirInfo;
    } else {
        // A file has been selected:
        // shows the preview of the selected file.
        fileInfo = QFileInfo(selectedFile);
    }

    // loads the file
    loadFile(fileInfo);
}

void ContentManager::stopViewers()
{
    // It is used to close running threads,subprocesses, and other viewer-specific
    // things.
    if (m_documentViewer->isActiveViewer) {
        m_documentViewer->stopViewer();
    }
    if (m_imageViewer->isActiveViewer) {
        m_imageViewer->stopViewer();
    }
    if (m_containerViewer->isActiveViewer) {
        m_containerViewer->stopViewer();
    }
}

void ContentManager::loadFile(const QFileInfo &fileInfo)
{
    currentFileInfo = fileInfo;
    // stops possible running subprocesses/threads.
    stopViewers();

    // sets the index number of the file in the title bar.
    QString titleBarText;
    // If the selected file is the "root" folder, then the current index number is 0
    if (fileInfo == rootDirInfo) {
        titleBarText = QStringLiteral("[%1/%2]").arg(0).arg(m_files.size());
    } else {
        titleBarText = QStringLiteral("[%1/%2]").arg(m_files.indexOf(currentFileInfo.absoluteFilePath()) + 1).arg(m_files.size());
    }
    setWindowTitle(titleBarText);
    // sets the file name as the title in the "header bar"
    setFileName(currentFileInfo.fileName());

    // gets the extension
    QString extension = getFileExtension(currentFileInfo);
    // load the file with the correct viewer.
    if (currentFileInfo.isDir()) {
        m_containerViewer->loadFile(currentFileInfo, false);
        setCurrentViewer(QStringLiteral("container_viewer"));
    } else if (textExtensions.contains(extension)) {
        m_textViewer->loadFile(currentFileInfo);
        setCurrentViewer(QStringLiteral("text_viewer"));
    } else if (docExtensions.contains(extension)) {
        m_documentViewer->loadFile(currentFileInfo, tempDir.path());
        setCurrentViewer(QStringLiteral("document_viewer"));
    } else if (imgExtensions.contains(extension)) {
        m_imageViewer->loadFile(currentFileInfo, tempDir.path());
        setCurrentViewer(QStringLiteral("image_viewer"));
    } else if (mediaExtensions.contains(extension)) {
        m_videoViewer->loadFile(currentFileInfo);
        setCurrentViewer(QStringLiteral("video_viewer"));
    } else if (containerExtensions.contains(extension)) {
        m_containerViewer->loadFile(currentFileInfo, true);
        setCurrentViewer(QStringLiteral("container_viewer"));
    } else {
        setCurrentViewer(QStringLiteral("fallback_viewer"));
    }
}

QString ContentManager::getFileExtension(const QFileInfo &fileInfo)
{
    QString extension = fileInfo.suffix().toLower();
    if (extension.isEmpty()) {
        // The extension is missing.
        QProcess process;
        // "file" is used to understand if is a text file or not.
        process.setProgram(QStringLiteral("file"));
        process.setArguments(QStringList() << QStringLiteral("--mime-type") << fileInfo.absoluteFilePath());
        process.start();
        process.waitForFinished();
        QString output = QString::fromUtf8(process.readAll());

        int pos = output.indexOf(QStringLiteral(":"));

        if (pos != -1) {
            // Extract the substring after the character ':'
            QString subStr = output.mid(pos + 2);
            // Check if it is a text file
            if (subStr.startsWith(QStringLiteral("text/"))) {
                extension = QStringLiteral("txt");
            }
        }
    }

    return extension;
}

void ContentManager::openApp()
{
    // opens the file with the default application via xdg-open.
    QProcess process;
    // the output must be redirected to QProcess::nullDevice()
    // otherwise Qt applications will not start if the parent process is closed.
    // https://forum.qt.io/topic/129701/qprocess-startdetached-but-the-child-process-closes-when-the-parent-exits/2
    process.setStandardErrorFile(QProcess::nullDevice());
    process.setStandardInputFile(QProcess::nullDevice());
    process.setStandardOutputFile(QProcess::nullDevice());

    process.setProgram(QStringLiteral("xdg-open"));
    process.setArguments(QStringList() << currentFileInfo.absoluteFilePath());
    process.startDetached();

    Q_EMIT closeQml();
}

void ContentManager::closing()
{
    // when the app is closing.
    stopViewers();
}

TextViewer *ContentManager::textViewer() const
{
    return m_textViewer;
}

ImageViewer *ContentManager::imageViewer() const
{
    return m_imageViewer;
}

DocumentViewer *ContentManager::documentViewer() const
{
    return m_documentViewer;
}

QString ContentManager::currentViewer() const
{
    return m_currentViewer;
}

void ContentManager::setCurrentViewer(const QString &newCurrentViewer)
{
    if (m_currentViewer == newCurrentViewer)
        return;
    m_currentViewer = newCurrentViewer;
    Q_EMIT currentViewerChanged();
}

QString ContentManager::fileName() const
{
    return m_fileName;
}

void ContentManager::setFileName(const QString &newFileName)
{
    if (m_fileName == newFileName)
        return;
    m_fileName = newFileName;
    Q_EMIT fileNameChanged();
}

VideoViewer *ContentManager::videoViewer() const
{
    return m_videoViewer;
}

ContainerViewer *ContentManager::containerViewer() const
{
    return m_containerViewer;
}

QString ContentManager::windowTitle() const
{
    return m_windowTitle;
}

void ContentManager::setWindowTitle(const QString &newWindowTitle)
{
    if (m_windowTitle == newWindowTitle)
        return;
    m_windowTitle = newWindowTitle;
    Q_EMIT windowTitleChanged();
}
