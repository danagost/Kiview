// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2024 danilo agostini <kyre334@gmail.com>
#ifndef DOCUMENTVIEWER_H
#define DOCUMENTVIEWER_H
#include <QFileInfo>
#include <QObject>
#include <QProcess>
class DocumentViewer : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString viewerDocument READ viewerDocument WRITE setViewerDocument NOTIFY viewerDocumentChanged FINAL)
    Q_PROPERTY(bool messagePageVisible READ messagePageVisible WRITE setMessagePageVisible NOTIFY messagePageVisibleChanged FINAL)
    Q_PROPERTY(QString messageHeaderText READ messageHeaderText WRITE setMessageHeaderText NOTIFY messageHeaderTextChanged FINAL)
    Q_PROPERTY(QString messageDescriptionText READ messageDescriptionText WRITE setMessageDescriptionText NOTIFY messageDescriptionTextChanged FINAL)
    Q_PROPERTY(bool reloadButtonVisible READ reloadButtonVisible WRITE setReloadButtonVisible NOTIFY reloadButtonVisibleChanged FINAL)
    Q_PROPERTY(bool loadingBarVisible READ loadingBarVisible WRITE setLoadingBarVisible NOTIFY loadingBarVisibleChanged FINAL)
    Q_PROPERTY(bool helpPageLinkVisible READ helpPageLinkVisible WRITE setHelpPageLinkVisible NOTIFY helpPageLinkVisibleChanged FINAL)

public:
    enum LoadingError { NoError, Timeout, UnknownError, FileNotFound, MissingLibreoffice, ErrorConverting };
    Q_ENUM(LoadingError)

    explicit DocumentViewer(QObject *parent = nullptr);
    void loadFile(const QFileInfo &fileInfo, const QString &tempDir = QString());
    QString viewerDocument() const;
    void setViewerDocument(const QString &newViewerDocument);

    int currentProcessId;
    bool processIsRunning = false;
    bool FirstLoading = true;

    QString currentFilePath;
    void loadingFailed(LoadingError errorCode, QString customError = QString());
    void closeActiveConversionProcess();
    void stopViewer();
    Q_INVOKABLE void reloadDocument();
    bool isActiveViewer = false;
    void convertDocument(QString path, QString outputDir);

    void conversionFinished(QFileInfo pdfFileInfo);

    QString messageHeaderText() const;
    void setMessageHeaderText(const QString &newMessageHeaderText);

    bool messagePageVisible() const;
    void setMessagePageVisible(bool newMessagePageVisible);

    QString messageDescriptionText() const;
    void setMessageDescriptionText(const QString &newMessageDescriptionText);

    bool reloadButtonVisible() const;
    void setReloadButtonVisible(bool newReloadButtonVisible);

    bool loadingBarVisible() const;
    void setLoadingBarVisible(bool newLoadingBarVisible);

    bool helpPageLinkVisible() const;
    void setHelpPageLinkVisible(bool newHelpPageLinkVisible);

Q_SIGNALS:

    void viewerDocumentChanged();
    void messageHeaderTextChanged();

    void messagePageVisibleChanged();

    void messageDescriptionTextChanged();

    void reloadButtonVisibleChanged();

    void loadingBarVisibleChanged();

    void helpPageLinkVisibleChanged();

private:
    QString getLibreofficeCommand();
    QString libreofficeCommand;
    void showLoadingScreen();
    QString m_viewerDocument;
    QString m_messageHeaderText;
    bool m_messagePageVisible;
    QString m_messageDescriptionText;
    bool m_reloadButtonVisible;
    bool m_loadingBarVisible;
    bool m_helpPageLinkVisible;
    bool isLibreofficeFlatpak = false;
};

#endif // DOCUMENTVIEWER_H
