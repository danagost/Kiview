// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2024 danilo agostini <kyre334@gmail.com>
#include "containerviewer.h"
#include <filesystem>
#include <klocalizedstring.h>
#include <math.h>
#include <signal.h>
#include <sys/stat.h>
#include <tuple>

// Kde
#include <K7Zip>
#include <KTar>
#include <KZip>

// Qt
#include <QDateTime>
#include <QDebug>
#include <QFileInfo>
#include <QThreadPool>

ContainerViewer::ContainerViewer(QObject *parent)
    : QObject{parent}
{
}

void ContainerViewer::loadFile(const QFileInfo &fileInfo, bool isArchive)
{
    resetView();
    // it is used to discard the thread if not longer needed
    isActiveViewer = true;
    currentFilePath = fileInfo.absoluteFilePath();

    // gets last modified time
    const QDateTime lastModified = fileInfo.lastModified();
    Qt::DateFormat format = Qt::DateFormat(QLocale::ShortFormat);
    QString stringLastModified = lastModified.date().toString(format);
    // sets the info in the interface
    setFileLastModified(stringLastModified);

    if (isArchive) {
        setFileIcon(QStringLiteral("application-zip"));
        // Runs the function in another thread to avoid freezing the interface
        QThreadPool::globalInstance()->start([this, &fileInfo]() {
            loadArchive(fileInfo);
        });
    } else {
        setFileIcon(QStringLiteral("folder-blue"));
        loadDir(currentFilePath);
    }
}

void ContainerViewer::loadDir(const QString &directoryPath)
{
    // the list containing files and directories that is passed to Qml.
    QList<ElementItem> elementsList;
    QDir directory = QDir(directoryPath);
    // searches for files and folders in the given folder
    directory.setFilter(QDir::Dirs | QDir::Files | QDir::NoDot | QDir::NoDotDot);
    directory.setSorting(QDir::Name | QDir::DirsFirst);
    for (const QFileInfo &entry : directory.entryInfoList()) {
        if (entry.isHidden()) {
            // excludes hidden folders from the list.
            continue;
        }

        // the object that will contain the information about the element and will be accessed by qml.
        ElementItem element;
        element.setPath(entry.fileName());

        if (entry.isDir()) {
            element.setFileType(ElementItem::Dir);
        } else if (entry.isSymLink()) {
            element.setFileType(ElementItem::Symlink);
        } else {
            element.setFileType(ElementItem::File);
        }

        elementsList.append(element);
    }

    setLoadingBarVisible(false);
    // sets the elements in the interface
    setViewerElements(elementsList);
    setFileContentCount(QString::number(elementsList.count()));
    return;
}

void ContainerViewer::loadArchive(const QFileInfo &fileInfo)
{
    QString path = fileInfo.absoluteFilePath();

    // used to discard the thread if no longer needed.
    auto shouldContinue = [this, &path]() {
        return (path == currentFilePath && isActiveViewer);
    };

    // gets the size
    qint64 size = fileInfo.size();
    // convert to new size
    const auto [newSize, unit] = convertFileSize(size);
    // set
    setFileSize(QString::number(newSize));
    setFileSizeUnit(unit);

    // avoid opening the archive if it is too big.
    if (size > 1073741824) { // 1gib
        // file too big
        qInfo() << "Preview not available: File too big";
        loadingFailed(i18n("Preview not available: File too big"));
        return;
    }

    QStringList contentList;
    QString extension = fileInfo.suffix();

    // A lambda is used to avoid writing the same code twice.
    auto readArchive = [&](auto &archiveEntry) {
        struct result {
            QString error;
            QStringList content;
        };
        // opens the archive.
        if (archiveEntry.open(QIODevice::ReadOnly)) {
            // Take the root folder from the archive and create a KArchiveDirectory object.
            const KArchiveDirectory *root = archiveEntry.directory();
            // returns the list of files in the archive.
            return result{QStringLiteral(), recursiveListEntries(root)};
        } else {
            qWarning() << "The archive could not be opened";
            return result{i18n("Error: %1", QStringLiteral("The archive could not be loaded")), QStringList()};
        }
    };

    // Opens the archive according to the extension.
    if (extension == QLatin1String("zip")) {
        KZip archive(path);
        auto result = readArchive(archive);
        if (!shouldContinue()) {
            return;
        }
        if (!result.error.isEmpty()) {
            loadingFailed(result.error);
            return;
        }
        contentList = result.content;
        archive.close();
    } else if (extension == QLatin1String("7z")) {
        K7Zip archive(path);
        auto result = readArchive(archive);
        if (!shouldContinue()) {
            return;
        }
        if (!result.error.isEmpty()) {
            loadingFailed(result.error);
            return;
        }
        contentList = result.content;
        archive.close();
    } else if (extension.endsWith(QStringLiteral("gz")) || extension.endsWith(QStringLiteral("xz"))) {
        KTar archive(path);
        auto result = readArchive(archive);
        if (!shouldContinue()) {
            return;
        }
        if (!result.error.isEmpty()) {
            loadingFailed(result.error);
            return;
        }
        contentList = result.content;
        archive.close();
    }

    if (!shouldContinue()) {
        return;
    }
    // removes empty strings added to the list.
    contentList.removeAll(QStringLiteral(""));

    // creates an element for each string in the list
    // this element is then passed to Qml
    QList<ElementItem> elementsList;
    for (const QString &name : contentList) {
        ElementItem element;
        element.setPath(name);
        // The list only contains files.
        element.setFileType(ElementItem::File);
        elementsList.append(element);
    }

    // sets the text in the UI
    setFileContentCount(QString::number(elementsList.count()));
    setViewerElements(elementsList);
    setLoadingBarVisible(false);
    return;
}

QStringList ContainerViewer::recursiveListEntries(const KArchiveDirectory *dir, const QString &path)
{
    QStringList fullContentList;
    QStringList entriesList = dir->entries();
    // recursively searches for files in the archive.
    for (const QString &item : std::as_const(entriesList)) {
        const KArchiveEntry *entry = dir->entry(item);

        if (entry->isFile()) {
            fullContentList.append(path + item);
        }
        // recursively searches for files in the folder.
        if (entry->isDirectory()) {
            fullContentList += recursiveListEntries((KArchiveDirectory *)entry, path + item + QStringLiteral("/"));
        }
    }
    fullContentList.sort();
    return fullContentList;
}

std::tuple<float, QString> ContainerViewer::convertFileSize(const qint64 &size)
{
    // convert bytes to KiB
    float convertedSize = size / 1024;
    // starting unit
    QString unit = QStringLiteral("KiB");

    if (convertedSize > 1024) {
        // convert KiB to MiB if more than 1024 KiB
        convertedSize = convertedSize / 1024;
        unit = QStringLiteral("MiB");
    }

    if (convertedSize > 1024) {
        // convert MiB to GiB if more than 1024 MiB
        convertedSize = convertedSize / 1024;
        unit = QStringLiteral("GiB");
    }

    // removes some numbers after the decimal point.
    float convertedSizeTruncated = float(round(convertedSize * 100)) / 100;
    return std::make_tuple(convertedSizeTruncated, unit);
}

void ContainerViewer::resetView()
{
    setErrorMessageText(QStringLiteral(""));
    setViewerElements(QList<ElementItem>());
    setFileLastModified(QStringLiteral(""));
    setFileContentCount(QStringLiteral(""));
    setFileSize(QStringLiteral(""));
    setLoadingBarVisible(true);
}

void ContainerViewer::loadingFailed(const QString &errorMessage)
{
    setLoadingBarVisible(false);
    setErrorMessageText(errorMessage);
}

void ContainerViewer::stopViewer()
{
    isActiveViewer = false;
}

QString ContainerViewer::fileIcon() const
{
    return m_fileIcon;
}

void ContainerViewer::setFileIcon(const QString &newFileIcon)
{
    if (m_fileIcon == newFileIcon)
        return;
    m_fileIcon = newFileIcon;
    Q_EMIT fileIconChanged();
}

QString ContainerViewer::fileSize() const
{
    return m_fileSize;
}

void ContainerViewer::setFileSize(const QString &newFileSize)
{
    if (m_fileSize == newFileSize)
        return;
    m_fileSize = newFileSize;
    Q_EMIT fileSizeChanged();
}

QString ContainerViewer::fileSizeUnit() const
{
    return m_fileSizeUnit;
}

void ContainerViewer::setFileSizeUnit(const QString &newFileSizeUnit)
{
    if (m_fileSizeUnit == newFileSizeUnit)
        return;
    m_fileSizeUnit = newFileSizeUnit;
    Q_EMIT fileSizeUnitChanged();
}

QString ContainerViewer::fileContentCount() const
{
    return m_fileContentCount;
}

void ContainerViewer::setFileContentCount(const QString &newFileContentCount)
{
    if (m_fileContentCount == newFileContentCount)
        return;
    m_fileContentCount = newFileContentCount;
    Q_EMIT fileContentCountChanged();
}

QString ContainerViewer::fileLastModified() const
{
    return m_fileLastModified;
}

void ContainerViewer::setFileLastModified(const QString &newFileLastModified)
{
    if (m_fileLastModified == newFileLastModified)
        return;
    m_fileLastModified = newFileLastModified;
    Q_EMIT fileLastModifiedChanged();
}

QString ContainerViewer::errorMessageText() const
{
    return m_errorMessageText;
}

void ContainerViewer::setErrorMessageText(const QString &newErrorMessageText)
{
    if (m_errorMessageText == newErrorMessageText)
        return;
    m_errorMessageText = newErrorMessageText;
    Q_EMIT errorMessageTextChanged();
}

QList<ElementItem> ContainerViewer::viewerElements() const
{
    return m_viewerElements;
}

void ContainerViewer::setViewerElements(const QList<ElementItem> &newViewerElements)
{
    if (m_viewerElements == newViewerElements)
        return;
    m_viewerElements = newViewerElements;
    Q_EMIT viewerElementsChanged();
}

bool ContainerViewer::loadingBarVisible() const
{
    return m_loadingBarVisible;
}

void ContainerViewer::setLoadingBarVisible(bool newLoadingBarVisible)
{
    if (m_loadingBarVisible == newLoadingBarVisible)
        return;
    m_loadingBarVisible = newLoadingBarVisible;
    Q_EMIT loadingBarVisibleChanged();
}
