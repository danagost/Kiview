// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2024 danilo agostini <kyre334@gmail.com>
#include "imageviewer.h"
#include <filesystem>
#include <klocalizedstring.h>
#include <signal.h>

// Kde
#include <KZip>

// Qt
#include <QDebug>
#include <QtConcurrent>

ImageViewer::ImageViewer(QObject *parent)
    : QObject{parent}
{
}

void ImageViewer::loadFile(const QFileInfo &fileInfo, const QString &tempDir)
{
    setErrorMessageText(QStringLiteral(""));
    setViewerImage(QStringLiteral(""));
    // it is used to discard the thread if not longer needed
    isActiveViewer = true;
    currentFilePath = fileInfo.absoluteFilePath();
    // Checks whether the file type requires extraction.
    QString extension = fileInfo.suffix().toLower();
    if (extension == QLatin1String("kra")) {
        // uses QtConcurrent to start the function in another thread.
        QtConcurrent::run(&ImageViewer::extractPreview, this, fileInfo.absoluteFilePath(), tempDir);
    } else {
        setViewerImage(QStringLiteral("file://") + fileInfo.absoluteFilePath());
    }
}

void ImageViewer::extractPreview(QString path, QString outputDir)
{
    // opens the .kra file (zip archive) to extract the preview inside.
    KZip archive(path);
    // Open the archive
    if (!archive.open(QIODevice::ReadOnly)) {
        qWarning() << "The file failed to load";
        setErrorMessageText(i18n("The file failed to load"));
        return;
    }

    // Take the root folder from the archive and create a KArchiveDirectory object.
    const KArchiveDirectory *root = archive.directory();

    // extracts the image.
    root->file(QStringLiteral("mergedimage.png"))->copyTo(outputDir);
    archive.close();

    // used to discard the thread if no longer needed.
    if (path != currentFilePath || isActiveViewer == false) {
        return;
    }

    // loads the extracted file.
    QFileInfo extractedFile(outputDir + QStringLiteral("/mergedimage.png"));
    loadFile(extractedFile);
    return;
}
QString ImageViewer::viewerImage() const
{
    return m_viewerImage;
}

void ImageViewer::setViewerImage(const QString &newViewerImage)
{
    if (m_viewerImage == newViewerImage)
        return;
    m_viewerImage = newViewerImage;
    Q_EMIT viewerImageChanged();
}

void ImageViewer::stopViewer()
{
    isActiveViewer = false;
}

QString ImageViewer::errorMessageText() const
{
    return m_errorMessageText;
}

void ImageViewer::setErrorMessageText(const QString &newErrorMessageText)
{
    if (m_errorMessageText == newErrorMessageText)
        return;
    m_errorMessageText = newErrorMessageText;
    Q_EMIT errorMessageTextChanged();
}
