// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2024 danilo agostini <kyre334@gmail.com>
#include "textviewer.h"

// Qt
#include <QDebug>
#include <QString>

TextViewer::TextViewer(QObject *parent)
    : QObject{parent}
{
}

void TextViewer::loadFile(const QFileInfo &fileInfo)
{
    // reads the file
    QString text;
    QFile file(fileInfo.absoluteFilePath());
    if (!file.open(QIODevice::ReadOnly)) {
        qWarning() << "Error opening file:" << fileInfo.absoluteFilePath();
        return;
    }

    text = QString::fromUtf8(file.readAll());
    file.close();

    // sets the text
    setViewerText(text);
}

QString TextViewer::viewerText() const
{
    return m_viewerText;
}

void TextViewer::setViewerText(const QString &newViewerText)
{
    if (m_viewerText == newViewerText)
        return;
    m_viewerText = newViewerText;
    Q_EMIT viewerTextChanged();
}
