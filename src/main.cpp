// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: 2024 danilo agostini <kyre334@gmail.com>
#include <KDBusService>
#include <KLocalizedContext>
#include <KLocalizedString>
#include <QApplication>
#include <QIcon>
#include <QQmlApplicationEngine>
#include <QUrl>
#include <QtLogging>
#include <QtQml>
#include <contentmanager.h>
#include <dbusinterface.h>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts);

    QApplication app(argc, argv);

    KLocalizedString::setApplicationDomain("kiview");
    QCoreApplication::setOrganizationName(QStringLiteral("nyre"));
    QCoreApplication::setOrganizationDomain(QStringLiteral("nyre221.github.io"));
    QCoreApplication::setApplicationName(QStringLiteral("Kiview"));

    QQmlApplicationEngine engine;

    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));

    // ContentManager manages which viewer is active
    ContentManager manager;
    // DBus interface
    KDBusService service(KDBusService::Unique);
    // The dbus interface requires `manager` in order to load the data received via dbus.
    DBusInterface interface(&manager);
    // Command line options
    if (argc > 1) {
        QString firstArg = QString::fromStdString(argv[1]);
        // Kiview runs in the background and waits to receive data via DBus.
        if (firstArg == QStringLiteral("--daemon")) {
            interface.setIsDaemon(true);
            qInfo("Started in daemon mode...");
        } else {
            qCritical() << "error: Invalid argument";
            exit(1);
        }
    } else {
        qCritical() << "error: No arguments given.";
        exit(1);
    }

    // registers ContentManager as a singleton so it can be accessed from qml.
    qmlRegisterSingletonInstance<ContentManager>("CManager", 1, 0, "Manager", &manager);
    // registers DBusInterface as a singleton so it can be accessed from qml.
    qmlRegisterSingletonInstance<DBusInterface>("DBusInterface", 1, 0, "BusInterface", &interface);
    // Makes the enum accessible from qml.
    qmlRegisterUncreatableMetaObject(ElementItem::staticMetaObject,
                                     "ElementTypes",
                                     1,
                                     0,
                                     "ElementType",
                                     QStringLiteral("Not creatable: only provides access to the enum."));
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    QObject::connect(&app, SIGNAL(aboutToQuit()), &manager, SLOT(closing()));
    // sets the icon of the app
    app.setWindowIcon(QIcon::fromTheme(QStringLiteral("io.github.nyre221.kiview")));

    return app.exec();
}
