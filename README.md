<!--
    SPDX-License-Identifier: GPL-3.0-or-later
    SPDX-FileCopyrightText: 2024 Danilo Agostini <kyre334@gmail.com>
-->

#### This version of Kiview is still under development and currently has no integration with dolphin
users can use the **old** flatpak version for now: https://flathub.org/apps/io.github.nyre221.kiview


# Kiview  
Kiview gives the user the ability to quickly preview different file types without the need to launch the default application.

### Supported file types:
##### Text Files:
txt, sh, json, md
##### Documents:
pdf, doc, docx, odt, ods, xlsx, xls, csv, odp, ppt, pptx
##### Images:
png, jpg, jpeg, kra, svgz, svg, webp, jxl
##### Audio/Video Files:
mp4, mp3, webm, mkv, m4a, m4v
##### Archives:
zip, gz, xz, 7z

## Installation
You can use install.sh to install from source: [Link](https://invent.kde.org/danagost/Kiview/-/blob/master/Install.sh?ref_type=heads)  
These are the dependencies: [Link](https://invent.kde.org/danagost/Kiview/-/blob/master/requirements(DEV%20ONLY).txt?ref_type=heads)  

## How it works

### From the terminal:  
This shows the preview of the file:   
`kiview -s /home/user/some_file_or_folder`  
This shows an overview of the working directory:  
`kiview /home/user/some_folder`

**There is currently no integration with dolphin and this is the only way to use it.**

`-s` means selection and will be used by dolphin when the user selects a file or folder. If the user has not selected anything, then `-s` will not be used and dolphin/kiview will simply show an overview of the currently open folder in dolphin.

**In simple words:**  
This is like when you invoke the shortcut after selecting a file:  
`kiview -s /home/user/some_file_or_folder`  
This is like when you invoke the shortcut when nothing has been selected:  
`kiview /home/user/some_folder`  

#### Examples:
![Screenshot-30-11-2023-CET-9](https://github.com/Nyre221/Kiview/assets/104171042/4e06a068-3d51-4b57-adcf-f42693b92e18)

![Screenshot-30-11-2023-CET-10](https://github.com/Nyre221/Kiview/assets/104171042/37b1fb2c-5e89-48fb-b5cf-25750734fd14)

![screenshot1](https://github.com/Nyre221/Kiview/assets/104171042/69f0806b-7c17-487d-a5dd-e4a6892b2b31)


### If you want to support my work:
https://www.paypal.com/donate/?hosted_button_id=J7QU55MMUP4G4
