#! /usr/bin/env bash
#SPDX-License-Identifier: GPL-3.0-or-later
#SPDX-FileCopyrightText: 2024 danilo agostini <kyre334@gmail.com>
# cmake -B build/ -DCMAKE_INSTALL_PREFIX="~/kde5/usr"
cmake -B build/
cmake --build build/
# cmake --install build/
sudo cmake --install build/
